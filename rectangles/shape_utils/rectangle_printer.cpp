#include "rectangle_printer.h"
#include <sstream>

using namespace ShapeUtils;

std::string Rectangle2DPrinter::StringRepresentation() const
{
   std::ostringstream StringStreamRep;
   
   StringStreamRep << "{" << Shape_.Position().X << "," << Shape_.Position().Y << "," << Shape_.Size().Width << "," << Shape_.Size().Height << "}";
   
   return StringStreamRep.str();
}
