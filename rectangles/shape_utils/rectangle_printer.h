//
//  rectangle_printer.h
//  triangles
//
//  Created by Nicu on 28.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __RECTANGLE_PRINTER__H
#define __RECTANGLE_PRINTER__H

#include "shape_printer.h"

namespace ShapeUtils {
   
   class Rectangle2DPrinter : public Shape2DPrinter {
   public:
      
      Rectangle2DPrinter(const Shape::Shape2D& _Shape)
      : Shape2DPrinter(_Shape)
      {}
      
      virtual std::string StringRepresentation() const;
      
   };
   
}


#endif
