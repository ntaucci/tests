//
//  shapebuilder_factory.hpp
//  triangles
//
//  Created by Nicu on 28.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __SHAPEBUILDER_FACTORY_H
#define __SHAPEBUILDER_FACTORY_H

#include "shape_builder.h"

namespace ShapeUtils {
   
   // **** ShapeBuilderFactory ****
   
   class ShapeBuilderFactory {
   public:
      
      static std::unique_ptr<ShapeBuilder2D> CreateShapeBuilder(ShapeType Type_);
      
   };
   
   // ----------------------------------------------------------
}


#endif
