
#include "shapebuilder_factory.h"

using namespace ShapeUtils;

std::unique_ptr<ShapeBuilder2D> ShapeBuilderFactory::CreateShapeBuilder(ShapeType Type_)
{
   return std::unique_ptr<ShapeBuilder2D>(new ShapeBuilder2D(Type_));
}