
#ifndef __SHAPE_BUILDER__H
#define __SHAPE_BUILDER__H

#include <memory>
#include <vector>

#include "../shape/rectangle.h"


namespace ShapeUtils {

   enum ShapeType {
      RECTANGLE_2D
   };
   
   
   // **** ShapeBuilder2D ****
   
   class ShapeBuilder2D {
   public:
      
      ShapeBuilder2D(ShapeType _Type)
      : Type_(_Type)
      {}
      
      ShapeBuilder2D& SetPosition(Math::Position2D _Position) { Position_ = _Position; return *this; };
      
      ShapeBuilder2D& SetSize(Math::RectangularSize2D _Size) { Size_ = _Size; return *this; };
      
      ShapeBuilder2D& SetID(int _ID) { ID_ = _ID; return *this; };
      
      std::unique_ptr<Shape::Shape2D> Build();
      
      typedef std::unique_ptr<Shape::Shape2D> (ShapeBuilder2D::*HandlerType)() const;
      
   private:
      std::unique_ptr<Shape::Shape2D> BuildRectangle2D_() const;
      
      void InitBuildHandlers_();
      
   private:
      
      Math::Position2D Position_;
      
      Math::RectangularSize2D Size_;
      
      int ID_ = -1;
      
      ShapeType Type_;
      
      static std::vector<HandlerType> BuildHandlers_;
      
      friend class ShapeBuilderInitializer;
      
   };
   
   // ---------------------------------------------------------------------------
   
   // **** ShapeBuilderInitializer ***
   
   class ShapeBuilderInitializer {
   public:
      ShapeBuilderInitializer();
   };
}


#endif
