#include "shape_builder.h"

using namespace ShapeUtils;

std::vector<ShapeBuilder2D::HandlerType> ShapeBuilder2D::BuildHandlers_;

void ShapeBuilder2D::InitBuildHandlers_()
{
   BuildHandlers_.push_back(&ShapeBuilder2D::BuildRectangle2D_);
}

std::unique_ptr<Shape::Shape2D> ShapeBuilder2D::Build()
{
   return (this->*BuildHandlers_[Type_])();
}

std::unique_ptr<Shape::Shape2D> ShapeBuilder2D::BuildRectangle2D_() const
{
   return std::unique_ptr<Shape::Rectangle2D>(new Shape::Rectangle2D(Position_, Size_, ID_));
}


ShapeBuilderInitializer::ShapeBuilderInitializer()
{
   ShapeBuilder2D(RECTANGLE_2D).InitBuildHandlers_();
}


static ShapeBuilderInitializer InitializeBuilderHandlers;

