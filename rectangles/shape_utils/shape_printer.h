//
//  shape_printer.h
//  triangles
//
//  Created by Nicu on 28.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __SHAPE_PRINTER__h
#define __SHAPE_PRINTER__h

#include <memory>
#include <string>

#include "../shape/shape.h"

namespace ShapeUtils {
   
   // **** Shape2DPrinter ****
   
   class Shape2DPrinter {
   public:
      
      Shape2DPrinter(const Shape::Shape2D& _Shape)
      : Shape_(_Shape)
      {}
      
      friend std::ostream& operator<<(std::ostream& ostr, const Shape2DPrinter& ShapePrinter);

      virtual std::string StringRepresentation() const = 0;
      
   protected:
      
      const Shape::Shape2D& Shape_;
      
   };
   
   // Shape2DPrinter inlines
   
   inline std::ostream& operator<<(std::ostream& ostr, const Shape2DPrinter& ShapePrinter)
   {
      ostr << ShapePrinter.StringRepresentation();
      
      return ostr;
   }
   
   // ----------------------------------------------------------------
}

#endif
