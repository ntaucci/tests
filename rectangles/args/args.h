#ifndef __ARGS__H_
#define __ARGS__H_

#include <map>
#include <set>
#include <string>
#include <memory>
#include <vector>

#include "args_exception.h"

// **** Argument ****

class Argument {
public:
	virtual void setValue(const std::string& ) = 0;
};

class StringArgument:public Argument {
public:
	virtual void setValue(const std::string&);

	friend class StringValueAccesor;
	class StringValueAccesor {
	public:

		static std::string getStringValue(std::shared_ptr<Argument> argument);

	};

private:
	std::string stringValue;
};

// -----------------------------------------------------------

// **** ArgsRange ****

struct ArgsRange {

	ArgsRange(const char** _begin, const char** _end)
		: begin(_begin)
		, end(_end)
	{}

	const char** begin;

	const char** end;
};

// -------------------------------------------------------------

// **** Args ****

class Args {
public:

	Args(std::string schema, ArgsRange range);

	std::string getString(char arg);

private:
	void parseSchema(std::string schema);

	void parseArgumentStrings(ArgsRange range);

	std::vector<std::string> split(const std::string& schema, char delimiter = ',');

	void parseSchemaElement(std::string element);

	void validateSchemaElementId(char elementId);

	void parseArgumentCharacters(std::string argChars);

	void parseArgumentCharacter(char argChar);

private:
	std::map<char, std::shared_ptr<Argument>> arguments;
	std::set<char> foundArguments;
	std::string currentArgument;
};

// ---------------------------------------------------------------

#endif