#include "args_exception.h"

#include <iostream>
#include <sstream>

const char* ArgsErrorString[] = {
	"OK",
	"INVALID_ARGUMENT_FORMAT",
	"UNEXPECTED_ARGUMENT",
	"INVALID_ARGUMENT_NAME",
	"MISSING_STRING",
	"MISSING_INTEGER",
	"INVALID_INTEGER",
	"MISSING_DOUBLE",
	"INVALID_DOUBLE"
};

std::ostream& operator<<(std::ostream& ostr, ArgsErrorCode errorCode)
{
	return (ostr << ArgsErrorString[errorCode]);
}

std::string ArgsException::message()
{
	std::ostringstream ostr;

	switch (errorCode) {

	case OK:
		ostr << ("This should not go here");
		break;

	case UNEXPECTED_ARGUMENT:
		ostr << "Argument " << errorArgumentId << " unexpected.";
		break;

	case MISSING_STRING:
		ostr << "Could not find string parameter for " << errorArgumentId;
		break;

	case INVALID_INTEGER:
		ostr << "Argument " << errorArgumentId << " expects an integer but was '" << errorParameter << "'";
		break;

	case MISSING_INTEGER:
		ostr << "Could not find integer parameter for " << errorArgumentId;
		break;

	case INVALID_DOUBLE:
		ostr << "Argument " << errorArgumentId << " expects a double but was '" << errorParameter << "'";
		break;

	case MISSING_DOUBLE:
		ostr << "Could not find double parameter for " << errorArgumentId;
		break;

	case INVALID_ARGUMENT_NAME:
		ostr << errorArgumentId << " is not a valid argument name";
		break;

	case INVALID_ARGUMENT_FORMAT:
		ostr << errorParameter << " is not a valid argument format";
		break;

	}

	return ostr.str();
}