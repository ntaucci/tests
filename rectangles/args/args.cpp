#include "args.h"

#include <iostream>
#include <sstream>

#include "args_exception.h"

void StringArgument::setValue(const std::string& _value)
{
	if (_value.empty()) throw ArgsException(MISSING_STRING);

	stringValue = _value;
}

std::string StringArgument::StringValueAccesor::getStringValue(std::shared_ptr<Argument> argument)
{
	if (argument != nullptr) {
		StringArgument* stringArgument = dynamic_cast<StringArgument*>(argument.get());
		if (stringArgument) return stringArgument->stringValue;
	}

	return "";
}

Args::Args(std::string schema, ArgsRange range)
{
	parseSchema(schema);
	parseArgumentStrings(range);
}

void Args::parseSchema(std::string schema)
{
	std::vector<std::string> delimitedStrings = split(schema);
	for (std::vector<std::string>::iterator it = delimitedStrings.begin(); it != delimitedStrings.end(); ++it) {
		if (!it->empty()) parseSchemaElement(*it);
	}
}

std::vector<std::string> Args::split(const std::string& schema, char delimiter)
{
	std::stringstream ss(schema);
    std::string item;
	std::vector<std::string> delimitedStrings;

    while (std::getline(ss, item, delimiter)) {
        delimitedStrings.push_back(item);
    }

    return delimitedStrings;
}

void Args::parseSchemaElement(std::string element)
{
	char elementID = element[0];
	std::string elementTail = element.substr(1);
	validateSchemaElementId(elementID);

	if (elementTail == "*") arguments[elementID] = std::make_shared<StringArgument>();
	else throw ArgsException(INVALID_ARGUMENT_FORMAT, elementID, elementTail);
}

void Args::validateSchemaElementId(char elementID)
{
	if (!isalpha(elementID)) throw ArgsException(INVALID_ARGUMENT_NAME, elementID);
}

void Args::parseArgumentStrings(ArgsRange range)
{
	for (currentArgument = *range.begin; range.begin != range.end; ++range.begin) {
		std::string argString(*range.begin);
		if (argString[0] == '-') {
			currentArgument = *(range.begin + 1);
			parseArgumentCharacters(argString.substr(1));
		}
	}
}

void Args::parseArgumentCharacters(std::string argChars)
{
	for (std::string::iterator it = argChars.begin(); it != argChars.end(); ++it) {
		parseArgumentCharacter(*it);
	}
}

void Args::parseArgumentCharacter(char argChar)
{
	std::shared_ptr<Argument> argument = nullptr;
	if (arguments.count(argChar)) {
		argument = arguments[argChar];
		foundArguments.insert(argChar);
	} else throw ArgsException(UNEXPECTED_ARGUMENT, argChar);

	try {
		argument->setValue(currentArgument);
	} catch (ArgsException& ex) {
		ex.setErrorArgumentID(argChar);
		throw ex;
	}
}

std::string Args::getString(char arg)
{
	return StringArgument::StringValueAccesor::getStringValue(arguments[arg]);
}