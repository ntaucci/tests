#ifndef __ARGS__EXCEPTION__H_
#define __ARGS__EXCEPTION__H_

#include <exception>
#include <string>

enum ArgsErrorCode {
	OK, 
	INVALID_ARGUMENT_FORMAT, 
	UNEXPECTED_ARGUMENT, 
	INVALID_ARGUMENT_NAME,
	MISSING_STRING,
	MISSING_INTEGER, 
	INVALID_INTEGER,
	MISSING_DOUBLE, 
	INVALID_DOUBLE
};

// **** AgrsException ****

class ArgsException:public std::exception {
public:

	ArgsException(ArgsErrorCode _errorCode = OK, char _errorArgumentId = '\0', std::string _errorParameter = std::string());

	std::string message();

	void setErrorArgumentID(char errorID);

	void setErrorCode(ArgsErrorCode _errorCode);

	void setErrorParameter(std::string _errorParameter);
   
   virtual const char* what() { return message().c_str(); }

private:
	ArgsErrorCode errorCode;
	char errorArgumentId;
	std::string errorParameter;
	
};

inline ArgsException::ArgsException(ArgsErrorCode _errorCode, char _errorArgumentId, std::string _errorParameter)
	: errorCode(_errorCode)
	, errorArgumentId(_errorArgumentId)
	, errorParameter(_errorParameter)
{
}

inline void ArgsException::setErrorArgumentID(char errorID)
{
	errorArgumentId = errorID;
}

inline void ArgsException::setErrorCode(ArgsErrorCode _errorCode)
{
	errorCode = _errorCode;
}

inline void ArgsException::setErrorParameter(std::string _errorParameter)
{
	errorParameter = _errorParameter;
}

#endif