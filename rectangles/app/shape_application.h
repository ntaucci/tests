#ifndef __SHAPE_APP__H
#define __SHAPE_APP__H

#include <vector>
#include <unordered_map>
#include <exception>
#include <string>
#include <set>

#include "application.h"
#include "../shape/shapes_overlap_region.h"
#include "../3rdparty/json.hpp"

// **** ApplicationException ****

class ApplicationException: public std::exception {
public:
   
   ApplicationException(const std::string& _ExceptionMessage)
   : ExceptionMessage_(_ExceptionMessage)
   {}
   
   virtual const char* what() { return ExceptionMessage_.c_str(); }
   
private:
   
   std::string ExceptionMessage_;
   
};

// ----------------------------------------------------------

// **** ShapeApplication ****

class ShapeApplication : public Application {
public:
   
   ShapeApplication(const Args& _AppArguments)
   : Application(_AppArguments)
   , InitialRegionsSize_(0)
   {}
   
   virtual void Run();
   
private:
   
   void BuildRegionShapes_();
   
   void AddRegionFromJSONReference_(typename nlohmann::basic_json<>::reference _RectsArrayReference, int _ID);
   
   void AddRegion_(Math::Position2D _Position, Math::RectangularSize2D _Size, int ID);
   
   void AddRegionFromRectangle_(const Shape::Shape2D& _Rectangle);
   
   void ConstructOverlapRegions_();
   
   std::string RegionIDsToString_(const std::set<int>& RegionIDs, const std::string& separator = " ") const;
   
   void PrintResults_();
   
   void PrintInitialInputShapes_();
   
   void PrintOverlappedAreas_();
   
private:
   
   typedef std::vector<Shape::ShapesOverlapRegion>::size_type RegionsSizeType;
   
   std::vector<Shape::ShapesOverlapRegion> RegionShapes_;
   
   std::unordered_map<std::string, int> UniqueRegionsIDs_;
   
   std::unordered_map<std::string, int> UniqueRectangles_;
   
   RegionsSizeType InitialRegionsSize_;
};

// --------------------------------------------------------

void LaunchApplication(int argc, const char * argv[]);


#endif
