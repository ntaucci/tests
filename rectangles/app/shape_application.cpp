#include "shape_application.h"

#include <fstream>
#include "../shape_utils/shapebuilder_factory.h"
#include "../shape_utils/rectangle_printer.h"

using namespace std;

const std::string JSON_RECTS_KEY = "rects";
const std::string JSON_X_KEY = "x";
const std::string JSON_Y_KEY = "y";
const std::string JSON_WIDTH_KEY = "w";
const std::string JSON_HEIGHT_KEY = "h";

// **** ApplicationRectanglePrinter ****

class ApplicationRectanglePrinter : public ShapeUtils::Shape2DPrinter {
public:
   
   ApplicationRectanglePrinter(const std::string& _Header, const Shape::Shape2D& _Shape)
   : ShapeUtils::Shape2DPrinter(_Shape)
   , Header_(_Header)
   {}
   
   std::string StringRepresentation() const;
   
   
private:
   
   std::string Header_;
   
};

std::string ApplicationRectanglePrinter::StringRepresentation() const
{
   std::ostringstream StringStreamRep;
   
   StringStreamRep <<"\t"<<Header_;
   StringStreamRep << " (";
   StringStreamRep << Shape_.Position().X << "," << Shape_.Position().Y;
   StringStreamRep << ")";
   StringStreamRep << ", " << "w=" << Shape_.Size().Width;
   StringStreamRep << ", " << "h=" << Shape_.Size().Height;
   
   return StringStreamRep.str();
}

// -------------------------------------

void ShapeApplication::Run()
{
   BuildRegionShapes_();
   
   InitialRegionsSize_ = RegionShapes_.size();
   
   ConstructOverlapRegions_();
   
   PrintResults_();
}

void ShapeApplication::BuildRegionShapes_()
{
   std::ifstream JsonFile(AppArguments_.getString('f'));
   
   if (!JsonFile) {
      throw ApplicationException("Wrong JSON file!");
   }
   
   nlohmann::json JSON;
   
   JSON << JsonFile;
   
   size_t RectanglesSize = JSON[JSON_RECTS_KEY].size();
   
   for (size_t i = 0; i < RectanglesSize; ++i) {
      int ID = static_cast<int>(i + 1);
      AddRegionFromJSONReference_(JSON[JSON_RECTS_KEY][i], ID);
   }
}

void ShapeApplication::AddRegionFromJSONReference_(typename nlohmann::basic_json<>::reference _RectsArrayReference, int _ID)
{
   Math::Position2D RegionPosition = { _RectsArrayReference[JSON_X_KEY], _RectsArrayReference[JSON_Y_KEY] };

   Math::RectangularSize2D RegionSize = { _RectsArrayReference[JSON_WIDTH_KEY],  _RectsArrayReference[JSON_HEIGHT_KEY] };
   
   AddRegion_(RegionPosition, RegionSize, _ID);
}

void ShapeApplication::AddRegion_(Math::Position2D _Position, Math::RectangularSize2D _Size, int ID)
{
   std::unique_ptr<Shape::Shape2D> RectangleShape = ShapeUtils::ShapeBuilderFactory::CreateShapeBuilder(ShapeUtils::RECTANGLE_2D)
                                                ->SetPosition(_Position)
                                                .SetSize(_Size)
                                                .SetID(ID)
                                                .Build();
   
   AddRegionFromRectangle_(*RectangleShape.get());
}

void ShapeApplication::AddRegionFromRectangle_(const Shape::Shape2D& _Rectangle)
{
   Shape::ShapesOverlapRegion OverlapRegion{_Rectangle};
   
   OverlapRegion.AddComposingRectangleID(_Rectangle.ID());
   
   RegionShapes_.push_back(OverlapRegion);
}


void ShapeApplication::ConstructOverlapRegions_()
{
   vector<Shape::ShapesOverlapRegion>::size_type RegionsSize = RegionShapes_.size();
   
   for (vector<Shape::ShapesOverlapRegion>::size_type i = 0; i < RegionsSize; ++i) {
      
      const Shape::ShapesOverlapRegion CurrentRegion(RegionShapes_[i]);
      const Shape::Shape2D& CurrentRectangle = CurrentRegion.GetRegionRectangle();
      
      for (vector<Shape::ShapesOverlapRegion>::size_type j = i + 1; j < RegionsSize; ++j) {
         
         const Shape::ShapesOverlapRegion SecondRegion(RegionShapes_[j]);
         const Shape::Shape2D& SecondRectangle = SecondRegion.GetRegionRectangle();
         
         if (!Shape::Shape2D::ShapesOverlap(CurrentRectangle, SecondRectangle)) continue;
         
         Shape::ShapesOverlapRegion OverlapRegion(*Shape::Shape2D::GetOverlapShape(CurrentRectangle, SecondRectangle).get());
         
         string ShapeString = ShapeUtils::Rectangle2DPrinter(OverlapRegion.GetRegionRectangle()).StringRepresentation();
         
         if (UniqueRectangles_[ShapeString] == 1) continue;
         
         UniqueRectangles_[ShapeString] = 1;
         
         for (int ID: CurrentRegion.GetComposingRecangleIDs()) OverlapRegion.AddComposingRectangleID(ID);
         
         for (int ID: SecondRegion.GetComposingRecangleIDs()) OverlapRegion.AddComposingRectangleID(ID);
         
         std::string IDsKey = RegionIDsToString_(OverlapRegion.GetComposingRecangleIDs(), "->");
         
         if (UniqueRegionsIDs_[IDsKey] == 0) {
            RegionShapes_.push_back(OverlapRegion);
            UniqueRegionsIDs_[IDsKey] = 1;
         }
      }
      
      if (RegionsSize == (i + 1)) RegionsSize = RegionShapes_.size();
   }
}

std::string ShapeApplication::RegionIDsToString_(const std::set<int>& RegionIDs, const std::string& separator) const
{
   string StringRepresentation;
   
   for (set<int>::iterator it = RegionIDs.begin(); it != RegionIDs.end(); ++it) {
      StringRepresentation.append(to_string(*it));
      set<int>::iterator NextIterator = it;
      ++NextIterator;
      if ((NextIterator) != RegionIDs.end()) StringRepresentation.append(separator);
   }
   
   return StringRepresentation;
}

void ShapeApplication::PrintResults_()
{
   PrintInitialInputShapes_();
   
   cout << endl;
   
   PrintOverlappedAreas_();
}

void ShapeApplication::PrintInitialInputShapes_()
{
   cout << "Input:" << endl;
   
   for (RegionsSizeType i = 0; i < InitialRegionsSize_; ++i) {
      
      const Shape::Shape2D& Rectangle = RegionShapes_[i].GetRegionRectangle();
      
      string Header;
      Header.append(to_string(Rectangle.ID()));
      Header.append(": Rectangle at");
      
      cout << ApplicationRectanglePrinter(Header, Rectangle) << endl;
   }
   
}

void ShapeApplication::PrintOverlappedAreas_()
{
   cout << "Overlaps" << endl;
   
   for (RegionsSizeType i = InitialRegionsSize_; i < RegionShapes_.size(); ++i) {
      
      string Header = "Between rectangle ";
      Header.append(RegionIDsToString_(RegionShapes_[i].GetComposingRecangleIDs(), " and "));
      
      cout << ApplicationRectanglePrinter(Header, RegionShapes_[i].GetRegionRectangle()) << endl;
   }
}

// ------------------------------------------------------------------------------------------------------------

void LaunchApplication(int argc, const char * argv[])
{
   ArgsRange ApplicationAgumentsRange(argv + 1, argv + argc);
   Args Arguments("f*", ApplicationAgumentsRange);
   
   ShapeApplication ShapeApplication_(Arguments);
   ShapeApplication_.Run();
}

