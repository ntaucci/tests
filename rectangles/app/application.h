//
//  application.h
//  rectangles
//
//  Created by Nicu on 29.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __APPLICATION__H
#define __APPLICATION__H

#include "../args/args.h"

// **** Application ****

class Application {
public:
   
   Application(const Args& _AppArguments)
   : AppArguments_(_AppArguments)
   {}
   
   virtual void Run() = 0;
   
protected:
   
   Args AppArguments_;
   
};

// ---------------------------

#endif
