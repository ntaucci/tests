#include <iostream>

#include "app/shape_application.h"

void DisplayUsageHelp()
{

   const char* help = "Usage:\n"\
                        "\trectangles -f json_file_name\n"\
                        "Example:\n"\
                        "\trectangles -f test_rectangles.json\n";

   std::cout << help << std::endl;

}

void CheckArguments(int argc)
{
   if (argc < 2) {
      
      DisplayUsageHelp();
      
      exit(1);
   }
}

int main(int argc, const char * argv[]) {
   
   CheckArguments(argc);
   
   try {
      
      LaunchApplication(argc, argv);
      
   } catch(ArgsException& ArgExc) {
      
      std::cerr << ArgExc.message() << std::endl;
      
      DisplayUsageHelp();
      
   } catch(ApplicationException& AppException) {
     
      std::cerr << AppException.what() << std::endl;
      
      DisplayUsageHelp();
      
   } catch (std::exception& STDException) {
      
      std::cerr << STDException.what() << std::endl;
      
   }
   
   exit(0);
}
