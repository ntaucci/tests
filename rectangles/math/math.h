#ifndef __MATH__H
#define __MATH__H

namespace Math {
   
   template<typename T>
   class Range {
   public:
      Range(T _LowerBoundary, T _UpperBoundary)
      : LowerBoundary(_LowerBoundary)
      , UpperBoundary(_UpperBoundary)
      {}
      
   private:
      
      T LowerBoundary;
      
      T UpperBoundary;
   };
   
   template<typename T>
   bool IsValueInRange(const Range<T>& range, T value)
   {
      return (value > range.LowerBoundary) && (value < range.UpperBoundary);
   }
   
   
   struct Position2D {
      
      Position2D(int _X = 0, int _Y = 0)
      : X(_X)
      , Y(_Y)
      {}
      
      int X;
      
      int Y;
      
   };
   
   struct RectangularSize2D {
      
      RectangularSize2D(int _Width = 0, int _Height = 0)
      : Width(_Width)
      , Height(_Height)
      {}
      
      int Width;
      
      int Height;
      
   };
   
}


#endif
