//
//  shapes_overlap_region.h
//  triangles
//
//  Created by Nicu on 28.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __SHAPES_OVERLAP_REGION__H
#define __SHAPES_OVERLAP_REGION__H

#include <set>
#include "rectangle.h"

namespace Shape {
   
   // **** ShapesOverlapRegion ****
   
   class ShapesOverlapRegion {
   public:
      
      ShapesOverlapRegion(const Shape::Shape2D& _RegionRectangle)
      : RegionRectangle_(_RegionRectangle.Clone())
      {}
      
      ShapesOverlapRegion(const ShapesOverlapRegion& Region)
      : ComposingRectangleIDs_(Region.ComposingRectangleIDs_)
      , RegionRectangle_(Region.RegionRectangle_->Clone())
      {
      }
      
      void AddComposingRectangleID(int ID) { ComposingRectangleIDs_.insert(ID); }
      
      std::set<int> GetComposingRecangleIDs() const { return ComposingRectangleIDs_; }
      
      const Shape::Shape2D& GetRegionRectangle() const { return *RegionRectangle_.get(); }
      
   private:
      
      std::set<int> ComposingRectangleIDs_;
      
      std::unique_ptr<Shape::Shape2D> RegionRectangle_;
      
   };
   
   // -------------------------------------------------------------------
   
}


#endif
