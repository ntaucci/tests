//
//  rectangle.h
//  triangles
//
//  Created by Nicu on 28.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __RECTANGLE__H
#define __RECTANGLE__H

#include "shape.h"

namespace Shape {
   
   class Rectangle2D : public Shape2D {
   public:
      
      Rectangle2D(Math::Position2D _Position, Math::RectangularSize2D _Size, int _ID = -1)
      : Shape2D(_Position, _Size, _ID)
      {}
      
      virtual Rectangle2D* Clone() const;
      
   private:
      
      virtual bool Overlaps_(const Shape2D& shape) const;
      
      bool OverlapHorizontally_(const Shape2D& shape) const;
      
      bool OverlapVertically_(const Shape2D& shape) const;
      
      virtual std::unique_ptr<Shape2D> OverlapShapeArea_(const Shape2D& shape) const;
      
      virtual bool IsShapeTheSame_(const Shape2D& shape) const;
      
   };
   
}


#endif
