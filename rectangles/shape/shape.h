//
//  shape.h
//  triangles
//
//  Created by Nicu on 28.08.2016.
//  Copyright © 2016 Taucci. All rights reserved.
//

#ifndef __SHAPE__H
#define __SHAPE__H

#include <iostream>
#include <memory>

#include "../math/math.h"

namespace Shape {
   
   // **** Shape2D ****
   
   class Shape2D {
   public:
      
      Shape2D(Math::Position2D _Position, Math::RectangularSize2D _Size, int _ID = -1)
      : Position_(_Position)
      , Size_(_Size)
      , ID_(_ID)
      {}
      
      Math::Position2D Position() const { return Position_; }
      
      void SetPosition(Math::Position2D _Position) { Position_ = _Position; }
      
      Math::RectangularSize2D Size() const { return Size_; }
      
      static bool ShapesOverlap(const Shape2D& shape1, const Shape2D& shape2);
      
      static std::unique_ptr<Shape2D> GetOverlapShape(const Shape2D& shape1, const Shape2D& shape2);
      
      bool operator==(const Shape2D& shape);
      
      bool operator!=(const Shape2D& shape);
      
      int ID() const { return ID_; }
      
      void SetID(int _ID) { ID_ = _ID; }
      
      virtual Shape2D* Clone() const = 0;
      
   private:
      
      virtual bool Overlaps_(const Shape2D& shape) const = 0;
      
      virtual std::unique_ptr<Shape2D> OverlapShapeArea_(const Shape2D& shape) const = 0;
      
      virtual bool IsShapeTheSame_(const Shape2D& shape) const = 0;
      
   protected:
     
      Math::Position2D Position_;
      
      Math::RectangularSize2D Size_;
      
      int ID_;
      
   };
   
   
    // Shape2D Inlines
   
   inline bool Shape2D::ShapesOverlap(const Shape2D& shape1, const Shape2D& shape2)
   {
      return shape1.Overlaps_(shape2);
   }
   
   inline std::unique_ptr<Shape2D> Shape2D::GetOverlapShape(const Shape2D& shape1, const Shape2D& shape2)
   {
      return shape1.OverlapShapeArea_(shape2);
   }
   
   inline bool Shape2D::operator==(const Shape2D& shape)
   {
      return IsShapeTheSame_(shape);
   }
   
   inline bool Shape2D::operator!=(const Shape2D& shape)
   {
      return !(*this == shape);
   }
   
   // -----------------------------------------------------------------------------------
   
}

#endif
