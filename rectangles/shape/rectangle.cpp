#include "rectangle.h"

using namespace Shape;

Rectangle2D* Rectangle2D::Clone() const
{
   return new Rectangle2D(*this);
}

bool Rectangle2D::Overlaps_(const Shape2D& shape) const
{
   return OverlapHorizontally_(shape) && OverlapVertically_(shape);
}

bool Rectangle2D::OverlapHorizontally_(const Shape2D& shape) const
{
   int HalfWidthSelf = Size_.Width / 2;
   int SelfMidX = Position_.X + HalfWidthSelf;
   
   int HalfWidthShape = shape.Size().Width / 2;
   int ShapeMidX = shape.Position().X + HalfWidthShape;
   
   int DistanceBetweenMidsH = HalfWidthSelf + HalfWidthShape;
   bool IsHorizontalOverlap = abs(SelfMidX - ShapeMidX) < DistanceBetweenMidsH;
   
   return IsHorizontalOverlap;
}

bool Rectangle2D::OverlapVertically_(const Shape2D& shape) const
{
   int HalfHeightSelf = Size_.Height / 2;
   int SelfMidY = Position_.Y + HalfHeightSelf;
   
   int HalfHeightShape = shape.Size().Height / 2;
   int ShapeMidY = shape.Position().Y + HalfHeightShape;
   
   int DistanceBetweenMidsV = HalfHeightSelf + HalfHeightShape;
   bool IsVerticalOverlap = abs(SelfMidY - ShapeMidY) < DistanceBetweenMidsV;
   
   return IsVerticalOverlap;
}

std::unique_ptr<Shape2D> Rectangle2D::OverlapShapeArea_(const Shape2D& shape) const
{
   int X = std::max(Position_.X, shape.Position().X);
   int Y = std::max(Position_.Y, shape.Position().Y);
   
   int SelfRightBottomCornerX = Position_.X + Size_.Width;
   int ShapeRightBottomCornerX = shape.Position().X + shape.Size().Width;
   
   int SelfRightBottomCornerY = Position_.Y + Size_.Height;
   int ShapeRightBottomCornerY = shape.Position().Y + shape.Size().Height;
   
   int MinWidth = std::min(SelfRightBottomCornerX, ShapeRightBottomCornerX);
   int MinHeigth = std::min(SelfRightBottomCornerY, ShapeRightBottomCornerY);
   
   int Width = MinWidth - X;
   int Height = MinHeigth - Y;
   
   return std::unique_ptr<Rectangle2D>(new Rectangle2D(Math::Position2D(X, Y), Math::RectangularSize2D(Width, Height)));
}

bool Rectangle2D::IsShapeTheSame_(const Shape2D& shape) const
{
   bool IsXTheSame = Position_.X == shape.Position().X;
   bool IsYTheSame = Position_.Y == shape.Position().Y;
   bool IsWidthTheSame = Size_.Width == shape.Size().Width;
   bool IsHeightTheSame = Size_.Height == shape.Size().Height;
   
   return (IsXTheSame && IsYTheSame && IsWidthTheSame && IsHeightTheSame);
}